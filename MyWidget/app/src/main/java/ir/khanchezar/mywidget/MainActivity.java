package ir.khanchezar.mywidget;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn1 = (Button) findViewById(R.id.btn1);
        final TextView tv1 = (TextView) findViewById(R.id.tv1);
        final ImageView iv1 = (ImageView) findViewById(R.id.iv1);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv1.setText("Hello !!!");
            }
        });

        final CheckBox chk1 = (CheckBox) findViewById(R.id.chk1);
        chk1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean ic1) {
                //Log.i("change",String.valueOf(ic));
                if (ic1 = true) {
                    iv1.setAlpha(.4f);
                } else if (ic1 = false) {
                    iv1.setAlpha(1.0f);
                }
            }
        });

        final CheckBox chk2 = (CheckBox) findViewById(R.id.chk2);
        chk2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean ic2) {
                if (ic2 = true) {
                    iv1.setVisibility(View.INVISIBLE);
                } else if (ic2 = false) {
                    iv1.setVisibility(View.VISIBLE);
                }
            }
        });

        final CheckBox chk3 = (CheckBox) findViewById(R.id.chk3);
        chk3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean ic3) {
                if (ic3 = true) {
                    iv1.setVisibility(View.VISIBLE);
                    iv1.setAlpha(1.0f);
                    tv1.setText("");
                }
            }
        });

        Switch sw1 = (Switch) findViewById(R.id.sw1);
        sw1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean ic4) {
                //Log.i("change", String.valueOf(ic4));
                if (ic4 = true) {
                    Toast.makeText(MainActivity.this, "Switch is ON", Toast.LENGTH_SHORT).show();
                } else if (ic4 = false) {
                    Toast.makeText(MainActivity.this, "now Switch is OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WebView wv1 = (WebView) findViewById(R.id.wv1);
        wv1.loadUrl("http://www.google.com");

    }
}
